﻿using UnityEngine;
using System.Collections;

public class cameraMove : MonoBehaviour {

    public GameObject reference;
    public beeMove bee;
    public float camSpeed = 1.0f;

    Camera cam;
	// Use this for initialization
	void Start () {
        cam = GetComponent<Camera>();
        bee = reference.GetComponent<beeMove>();
    }
	
	// Update is called once per frame
	void Update () {
        bee = reference.GetComponent<beeMove>();
        if (bee != null)
        {
            if (getMaxX(bee.targets) - getMinX(bee.targets) > Screen.width / 3)
            {
                cam.orthographicSize += camSpeed * Time.deltaTime;
            }
            else if (getMaxX(bee.targets) - getMinX(bee.targets) < Screen.width / 4 && cam.orthographicSize > 5)
            {
                cam.orthographicSize -= camSpeed * Time.deltaTime;
            }

            if ((getMaxX(bee.targets) + getMinX(bee.targets)) / 2 > 7 * Screen.width / 13)
            {
                transform.Translate(camSpeed * Time.deltaTime, 0, 0);
            }
            else if ((getMaxX(bee.targets) + getMinX(bee.targets)) / 2 < 6 * Screen.width / 13)
            {
                transform.Translate(-camSpeed * Time.deltaTime, 0, 0);
            }

            if ((getMaxY(bee.targets) + getMinY(bee.targets)) / 2 > 7 * Screen.width / 13)
            {
                transform.Translate(0, camSpeed * Time.deltaTime, 0);
            }
            else if ((getMaxY(bee.targets) + getMinY(bee.targets)) / 2 < 6 * Screen.width / 13)
            {
                transform.Translate(0, -camSpeed * Time.deltaTime, 0);
            }
        }
	}

    int getMinX (Transform[] arr)
    {
        int x = Screen.width;
        for (int i = 0; i < arr.Length; i ++)
        {
            if (cam.WorldToScreenPoint(arr[i].position).x < x)
            {
                x = (int)cam.WorldToScreenPoint(arr[i].position).x;
            }
        }
        return x;
    }
    int getMaxX(Transform[] arr)
    {
        int x = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            if (cam.WorldToScreenPoint(arr[i].position).x > x)
            {
                x = (int)cam.WorldToScreenPoint(arr[i].position).x;
            }
        }
        return x;
    }
    int getMinY(Transform[] arr)
    {
        int y = Screen.height;
        for (int i = 0; i < arr.Length; i++)
        {
            if (cam.WorldToScreenPoint(arr[i].position).y < y)
            {
                y = (int)cam.WorldToScreenPoint(arr[i].position).y;
            }
        }
        return y;
    }
    int getMaxY(Transform[] arr)
    {
        int y = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            if (cam.WorldToScreenPoint(arr[i].position).y > y)
            {
                y = (int)cam.WorldToScreenPoint(arr[i].position).y;
            }
        }
        return y;
    }
}

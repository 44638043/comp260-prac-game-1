﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class beeMove : MonoBehaviour
{
    public float minSpeed = 10, maxSpeed = 18;
    public float minTurn = 90, maxTurn = 180;

    public float speed = 4.0f;
    public float turnSpeed = 180.0f;

    //public Transform target;

    public Transform[] targets;
    public Transform currentTarget;

    public Vector2 direction;
    public Vector2 heading = Vector3.right;
    
    public float minDist = 5.0f;
    public float maxDist;

    public ParticleSystem explosion;

    // Use this for initialization
    void Start()
    {
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurn, maxTurn, Random.value);

        explosion.enableEmission = false;
    }

    // Update is called once per frame
    void Update()
    {
        //test code for finding closest player
        targets = getTargets("player");
        if (targets != null)
        {
            // Get max dist, so the min dist has a distance to reset to
            for (int i = 0; i < targets.Length; i++)
            { 
                Vector2 directionTemp = targets[i].position - transform.position;
                if (directionTemp.magnitude > maxDist)
                {
                    maxDist = directionTemp.magnitude;
                }
            }
            minDist = maxDist; //reset the min dist, otherwise, when the min dist reaches the lowest, the bee can't change targets
            for (int i = 0; i < targets.Length; i++)
            {
                Vector2 directionTemp = targets[i].position - transform.position;
                Debug.Log("player" + i + " dist: " + directionTemp.magnitude);
                if (directionTemp.magnitude < minDist)
                {
                    minDist = directionTemp.magnitude;
                    currentTarget = targets[i];
                }
            }
        } else
        {
            Debug.Log(targets);
        }

        direction = currentTarget.position - transform.position;
        float angle = turnSpeed * Time.deltaTime;

        if (direction.IsOnLeft(heading))
        {
            heading = heading.Rotate(angle);
        }
        else
        {
            heading = heading.Rotate(-angle);
        }

        /*
        Vector2 velocity = direction * speed;
        transform.Translate(velocity * Time.deltaTime);
        */

        transform.Translate(heading * speed * Time.deltaTime);
    }
    /*
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        Gizmos.color = Color.yellow;
        for (int i = 0; i < targets.Length; i ++)
        {
            Vector2 direction = targets[i].position - transform.position;
            Gizmos.DrawRay(transform.position, direction);
        }
    }
    */

    Transform[] getTargets(string p)
    {
        GameObject[] gotPlayers = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        var list = new System.Collections.Generic.List<Transform>();

        for (int i = 0; i < gotPlayers.Length; i++)
        {
            if (gotPlayers[i].layer == LayerMask.NameToLayer(p))
            {
                list.Add(gotPlayers[i].GetComponent<Transform>());
            }
        }
        return list.ToArray();
    }

    void OnDestroy ()
    {
        ParticleSystem explode = Instantiate(explosion);

        explode.enableEmission = true;
        explode.transform.position = transform.position;

        Destroy(explode.gameObject, explode.duration);
    }

    public void setExplosion(ParticleSystem newExplosion)
    {
        explosion = newExplosion;
    }
}

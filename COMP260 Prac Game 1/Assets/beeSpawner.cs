﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class beeSpawner : MonoBehaviour {

    public float beeSeconds;

    public float beeSpawnTime;
    public float minPeriod, maxPeriod;
    public int beeCount = 0;

    public GameObject bee;
    public ParticleSystem explosion;

    public float xMin, yMin;
    public float width, height;

	// Use this for initialization
	void Start () {
        beeCount = 0;
        beeSpawnTime = Mathf.Lerp(minPeriod, maxPeriod, Random.value * 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
        beeSeconds += Time.deltaTime;
        if (beeSeconds > beeSpawnTime && transform.childCount < 15)
        {
            createBees();
            beeSeconds = 0;

            beeSpawnTime = Mathf.Lerp(minPeriod, maxPeriod, Random.value * 1.0f);
        }
	
	}

    public void createBees()
    {

        GameObject newBee = Instantiate(bee);

        newBee.transform.parent = transform;
        newBee.gameObject.name = "bee " + beeCount;
        beeCount++;

        float x = xMin + Random.value * width;
        float y = yMin + Random.value * height;

        bee.transform.position = new Vector2(x, y);

    }

    public void destroyBees(Vector2 center, float radius)
    {
        for (int i = 0; i < transform.childCount; i ++)
        {
            Transform child = transform.GetChild(i);

            Vector2 v = (Vector2)child.position - center;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }
}

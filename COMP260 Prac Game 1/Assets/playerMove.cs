﻿using UnityEngine;
using System.Collections;

public class playerMove : MonoBehaviour {

    public int player = 1;

    public Vector2 velocity;
    public float maxSpeed = 5.0f;

    public beeSpawner beeRef;
    public float destroy = 1.0f;

	// Use this for initialization
	void Start () {
        beeRef = FindObjectOfType<beeSpawner>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 direction;
        
        //A few test codes for detecting multiple players
        if (player == 1)
        {
            direction.x = Input.GetAxis("Horizontal");
            direction.y = Input.GetAxis("Vertical");
        } else
        {
            direction.x = Input.GetAxis("Horizontal2");
            direction.y = Input.GetAxis("Vertical2");
        }

        velocity = direction * maxSpeed;

        transform.Translate(direction);

        if (Input.GetButton("Fire1"))
        {
            beeRef.destroyBees(transform.position, destroy);
        }
	}
}
